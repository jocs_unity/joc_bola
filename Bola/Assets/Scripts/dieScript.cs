﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dieScript : MonoBehaviour
{
    private AudioSource audio;

    // Start is called before the first frame update
    void Start()
    {
        audio.Play();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Click()
    {
        Application.LoadLevel("First Example");
    }

    public void Exit()
    {
        Application.Quit();
    }
}