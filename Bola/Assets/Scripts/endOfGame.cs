﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class endOfGame : MonoBehaviour
{
//    private AudioSource audio;
    public Text timer;

    // Start is called before the first frame update
    void Start()
    {
//        audio.Play();
        timer.text = "Your total time is: "+ GameManager.temps;
    }

    // Update is called once per frame
    void Update()
    {
    }


    public void Exit()
    {
        Application.Quit();
    }
}